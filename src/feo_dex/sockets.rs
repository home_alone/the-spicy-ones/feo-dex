use pnet::datalink::{self, Channel::Ethernet, DataLinkReceiver, DataLinkSender, NetworkInterface};
use std::io;
use std::net::ToSocketAddrs;
pub use std::net::UdpSocket;

pub fn get_udp_socket<A: ToSocketAddrs>(
    local_port: u16,
    remote_addres: A,
) -> io::Result<UdpSocket> {
    let sock = UdpSocket::bind(format!("127.0.0.1:{}", local_port))?;
    sock.connect(remote_addres)?;
    Ok(sock)
}

pub fn get_raw_datalink_socket(if_name: &str) -> RawDatalinkSocket {
    let interfaces = datalink::interfaces();
    let interface = interfaces
        .into_iter()
        // Find the network interface with the provided name
        .filter(|iface: &NetworkInterface| iface.name == *if_name)
        .next()
        .unwrap();

    // For future use
    let mut conf: pnet::datalink::Config = Default::default();
    match datalink::channel(&interface, conf) {
        Ok(Ethernet(tx, rx)) => RawDatalinkSocket { tx: tx, rx: rx },
        Ok(_) => panic!("Unhandled channel type"),
        Err(e) => panic!(
            "An error occurred when creating the datalink channel: {}",
            e
        ),
    }
}

pub enum PipeResult {
    Success,
    ErrorOnSend(io::ErrorKind),
    ErrorOnReceive(io::ErrorKind),
}

pub trait BridgedSocket {
    fn receive(&mut self) -> io::Result<Box<[u8]>>;
    fn send(&mut self, buf: &[u8]) -> io::Result<()>;
    fn set_as_nonblocking(&mut self) -> io::Result<()>;

    /**
     * Sends a packet from the current socket to `target`.
     */
    #[inline]
    fn pipe_packet_to<S: BridgedSocket>(&mut self, target: &mut S) -> PipeResult {
        match self.receive() {
            Ok(packet) => match target.send(packet.as_ref()) {
                Ok(_) => PipeResult::Success,
                Err(err) => PipeResult::ErrorOnSend(err.kind()),
            },
            Err(err) => PipeResult::ErrorOnReceive(err.kind()),
        }
    }
}

pub struct RawDatalinkSocket {
    tx: Box<dyn DataLinkSender>,
    rx: Box<dyn DataLinkReceiver>,
}

impl BridgedSocket for RawDatalinkSocket {
    fn receive(&mut self) -> io::Result<Box<[u8]>> {
        match self.rx.next() {
            Ok(packet) => Ok(Box::from(packet)),
            Err(err) => Err(err),
        }
    }

    fn send(&mut self, packet: &[u8]) -> io::Result<()> {
        match self.tx.send_to(packet, None).unwrap() {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }

    fn set_as_nonblocking(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl BridgedSocket for UdpSocket {
    fn receive(&mut self) -> io::Result<Box<[u8]>> {
        let mut buf = [0; 9000];
        match self.recv(&mut buf) {
            Ok(len) => Ok(Box::from(&buf[..len])),
            Err(err) => Err(err),
        }
    }

    fn send(&mut self, packet: &[u8]) -> io::Result<()> {
        match Self::send(self, packet) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }

    fn set_as_nonblocking(&mut self) -> io::Result<()> {
        match self.set_nonblocking(true) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}
