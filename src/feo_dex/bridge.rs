use super::sockets::{BridgedSocket, PipeResult};
use std::io::ErrorKind;
use std::ops::AddAssign;
use std::time::{Duration, Instant};

#[derive(Debug)]
pub struct BridgeStats {
    pub id: u16,
    pub sent_a_to_b: u32,
    pub dropped_a_to_b: u32,
    pub sent_b_to_a: u32,
    pub dropped_b_to_a: u32,
}

impl BridgeStats {
    pub fn new(id: u16) -> BridgeStats {
        BridgeStats {
            id: id,
            sent_a_to_b: 0,
            dropped_a_to_b: 0,
            sent_b_to_a: 0,
            dropped_b_to_a: 0,
        }
    }
}

impl AddAssign for BridgeStats {
    fn add_assign(&mut self, rhs: Self) {
        self.sent_a_to_b += rhs.sent_a_to_b;
        self.dropped_a_to_b += rhs.dropped_a_to_b;
        self.sent_b_to_a += rhs.sent_b_to_a;
        self.dropped_b_to_a += rhs.dropped_b_to_a;
    }
}

pub struct Bridge<SA, SB>
where
    SA: BridgedSocket,
    SB: BridgedSocket,
{
    pub id: u16,
    sock_a: SA,
    sock_b: SB,
}

impl<SA, SB> Bridge<SA, SB>
where
    SA: BridgedSocket,
    SB: BridgedSocket,
{
    /**
     * The sockets passed need to be fully initialized and ready for usage.
     */
    pub fn new(id: u16, mut sock_a: SA, mut sock_b: SB) -> Bridge<SA, SB> {
        sock_a.set_as_nonblocking().unwrap();
        sock_b.set_as_nonblocking().unwrap();
        Bridge {
            id: id,
            sock_a: sock_a,
            sock_b: sock_b,
        }
    }

    pub fn run_for(&mut self, duration: Duration) -> BridgeStats {
        let mut run_stats = BridgeStats::new(self.id);
        let start = Instant::now();

        while start.elapsed() < duration {
            Self::update_stats_by_pipe_results(
                self.sock_a.pipe_packet_to(&mut self.sock_b),
                &mut run_stats.sent_a_to_b,
                &mut run_stats.dropped_a_to_b,
            );
            Self::update_stats_by_pipe_results(
                self.sock_b.pipe_packet_to(&mut self.sock_a),
                &mut run_stats.sent_b_to_a,
                &mut run_stats.dropped_b_to_a,
            );
        }

        run_stats
    }

    #[inline]
    fn update_stats_by_pipe_results(pipe_result: PipeResult, sent: &mut u32, dropped: &mut u32) {
        match pipe_result {
            PipeResult::Success => *sent += 1,
            PipeResult::ErrorOnReceive(reason) => match reason {
                ErrorKind::WouldBlock | ErrorKind::ConnectionRefused => (),
                _ => *dropped += 1,
            },
            PipeResult::ErrorOnSend(_) => *dropped += 1,
        }
    }
}
