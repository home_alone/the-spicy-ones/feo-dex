use super::bridge::{Bridge, BridgeStats};
use super::sockets::{BridgedSocket, UdpSocket};

pub type SpineBridge = Bridge<UdpSocket, UdpSocket>;
