use super::bridge::{Bridge, BridgeStats};
use super::sockets::{get_raw_datalink_socket, get_udp_socket, RawDatalinkSocket, UdpSocket};
use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;

static DEFAULT_BRIDGE_RUN_TIME: Duration = Duration::from_secs(1);

pub type LeafNetworkBridge = Bridge<RawDatalinkSocket, UdpSocket>;

pub struct LeafState {
    pub stats: HashMap<u16, BridgeStats>,
    handles: HashMap<u16, JoinHandle<()>>,
}

impl LeafState {
    pub fn new() -> LeafState {
        LeafState {
            stats: HashMap::new(),
            handles: HashMap::new(),
        }
    }

    pub fn update_stats(&mut self, stats: BridgeStats) {
        let net_stats = self.stats.get_mut(&stats.id).unwrap();
        *net_stats += stats;
    }

    pub fn init_net(
        &mut self,
        net_id: u16,
        if_name: String,
        spine_ip: &'static str,
        stats_w: mpsc::Sender<BridgeStats>,
    ) {
        self.stats.insert(net_id, BridgeStats::new(net_id));

        let handle = thread::spawn(move || {
            let mut network_bridge = LeafNetworkBridge::new(
                net_id,
                get_raw_datalink_socket(if_name.as_str()),
                get_udp_socket(net_id, format!("{}:{}", &spine_ip, net_id)).unwrap(),
            );
            loop {
                let stats = network_bridge.run_for(DEFAULT_BRIDGE_RUN_TIME);
                stats_w.send(stats).unwrap();
            }
        });

        self.handles.insert(net_id, handle);
    }

    pub fn remove_net(&mut self, net_id: &u16) {
        self.handles.remove(net_id).unwrap().join().unwrap();
        self.stats.remove(net_id);
    }
}
