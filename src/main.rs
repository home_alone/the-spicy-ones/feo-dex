mod feo_dex;

#[macro_use]
extern crate lazy_static;

use feo_dex::leaf::LeafState;
use std::env;
use std::sync::mpsc;

lazy_static! {
    static ref SPINE_IP: String = { env::args().skip(1).next().unwrap() };
}

fn main() {
    // ** This is a DEBUGGING Spine entrypoint, I use it to debug because udp sockets are **
    // ** easier to open compared to data link sockets on my current work environment.    **
    // use std::time::Duration;
    // use feo_dex::sockets::get_udp_socket;
    // use feo_dex::spine::SpineBridge;
    // use feo_dex::bridge::BridgeStats;
    // let mut b = SpineBridge::new(
    //     1,
    //     get_udp_socket(16000, format!("{}:{}", "127.0.0.1", 16001)).unwrap(),
    //     get_udp_socket(10000, format!("{}:{}", "127.0.0.1", 10001)).unwrap(),
    // );

    // let mut stats = BridgeStats::new(1);
    // loop {
    //     stats += b.run_for(Duration::from_secs(1));
    //     println!("{:?}", stats);
    // }

    let mut args = env::args().skip(2);
    if args.len() < 2 {
        panic!("Not enough arguments supplied!");
    }

    let mut state = LeafState::new();
    let (stats_w, stats_r) = mpsc::channel();
    while args.len() >= 2 {
        let if_name = args.next().unwrap();
        let net_id = args.next().unwrap().parse().unwrap();
        state.init_net(net_id, if_name, &SPINE_IP, stats_w.clone());
    }

    for stats in stats_r {
        state.update_stats(stats);
        println!("{:?}", state.stats)
    }
}
